app.controller("LicenciaIndexController", ["$scope", "$window", "$location", function ($scope, $window, $location) {

        // $scope.message = 'Holis, te Cuento QUE DARIO SE LA COME'

        $scope.licencias=[{fecha:"12/02/2017",nroLicencia:234765,agenteNombre:"Sanchez, Rigoberta Olivia",agenteNroDocuemnto:20345876, nroComprobante:123654},
        {fecha:"13/02/2017",nroLicencia:234765,agenteNombre:"Sanchez, Rigoberta Olivia",agenteNroDocuemnto:20345876, nroComprobante:123654},
        {fecha:"14/02/2017",nroLicencia:234766,agenteNombre:"Gomez, Ruviela Silvana",agenteNroDocuemnto:31234765, nroComprobante:123655},
        {fecha:"14/02/2017",nroLicencia:234767,agenteNombre:"Quico, Federico ",agenteNroDocuemnto:13234567, nroComprobante:123656},
        {fecha:"15/02/2017",nroLicencia:234768,agenteNombre:"Sanchez, Noten Ganches",agenteNroDocuemnto:12345876, nroComprobante:123657},
        {fecha:"15/02/2017",nroLicencia:234769,agenteNombre:"Gomez, Notec Opes",agenteNroDocuemnto:13244657, nroComprobante:123658},
        {fecha:"16/02/2017",nroLicencia:234770,agenteNombre:"Miranda, Ylaguita Radelolo",agenteNroDocuemnto:11234654, nroComprobante:123659}]; 

         $scope.personas=[{nombre:"Sanchez, Rigoberta Olivia",dni:20, direccion:"Carlos Gardel 365", telefono:"3624556987",Email:"direccion@email.com"},
        {nombre:"Gomez, Ruviela Silvana",dni:20, direccion:"Carlos Gardel 365", telefono:"3624556987",Email:"Banquito@email.com"},
        {nombre:"Banquito, Armando Esteban",dni:20, direccion:"Savedra 365", telefono:"3624556987",Email:"Flores@email.com"},
        {nombre:"Flores, Armando Ramos",dni:20, direccion:"Alvear 365", telefono:"3624556987",Email:"Gomez@email.com"},
        {nombre:"Delocho, Estees Elchavo",dni:20, direccion:"Cangallo 365", telefono:"3624556987",Email:"Delocho@email.com"},
        {nombre:"Quico, Federico",dni:20, direccion:"Echeverria 365", telefono:"3624556987",Email:"Quico@email.com"},
        {nombre:"Ochote, Abrocho",dni:20, direccion:"Posadas 365", telefono:"3624556987",Email:"Ochote@email.com"}]; 

        $scope.meses=[{nombre:"Enero",nro:01},
        {nombre:"Febrero",nro:02},
        {nombre:"Marzo",nro:03},
        {nombre:"Abril",nro:04},
        {nombre:"Mayo",nro:05},
        {nombre:"Junio",nro:06},
        {nombre:"Julio",nro:07},
        {nombre:"Agosto",nro:08},
        {nombre:"Septiembre",nro:09},
        {nombre:"Octubre",nro:10},
        {nombre:"Noviembre",nro:11},
        {nombre:"Diciembre",nro:12}]; 


        $scope.getMessage = function (){
            return $scope.message;
        };
    }]);
